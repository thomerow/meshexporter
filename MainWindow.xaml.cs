﻿//------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace MeshExporter {
   using System;
   using System.ComponentModel;
   using System.Diagnostics;
   using System.Globalization;
   using System.IO;
   using System.Windows;
   using System.Windows.Media;
   using System.Windows.Media.Imaging;
   using Microsoft.Kinect;
   using System.Threading.Tasks;
   using System.Threading;
   using System.Linq;
   using System.Runtime.InteropServices;
   using System.Collections.Generic;/// <summary>
                                    /// Interaction logic for MainWindow
                                    /// </summary>
   public partial class MainWindow : Window, INotifyPropertyChanged {

      /// <summary>
      /// Active Kinect sensor
      /// </summary>
      private KinectSensor _kinectSensor = null;

      /// <summary>
      /// Reader for depth frames
      /// </summary>
      private DepthFrameReader _depthFrameReader = null;

      /// <summary>
      /// Description of the data contained in the depth frame
      /// </summary>
      private FrameDescription _depthFrameDescription = null;

      /// <summary>
      /// Bitmap to display
      /// </summary>
      private WriteableBitmap _depthBitmap = null;

      /// <summary>
      /// Intermediate storage for frame data converted to color
      /// </summary>
      private byte[] _depthPixels = null;

      private CameraSpacePoint[] _camSpacePoints;

      private Vector3D[] _camSpaceAverage;

      /// <summary>
      /// Current status text to display
      /// </summary>
      private string _statusText = null;

      private int _maxDistance = 4500;

      private int _minDistance = 500;

      private double _certainty = 0.75;

      private double _evenLineCorrection = 0.0;

      /// <summary>
      /// Initializes a new instance of the MainWindow class.
      /// </summary>
      public MainWindow() {
         // get the kinectSensor object
         _kinectSensor = KinectSensor.GetDefault();

         // open the reader for the depth frames
         _depthFrameReader = _kinectSensor.DepthFrameSource.OpenReader();

         // wire handler for frame arrival
         _depthFrameReader.FrameArrived += DepthFrameArrived;

         // get FrameDescription from DepthFrameSource
         _depthFrameDescription = _kinectSensor.DepthFrameSource.FrameDescription;

         // Allocate array of 3D-coordinates
         _camSpacePoints = new CameraSpacePoint[_depthFrameDescription.Width * _depthFrameDescription.Height];
         _camSpaceAverage = new Vector3D[_depthFrameDescription.Width * _depthFrameDescription.Height];

         // allocate space to put the pixels being received and converted
         _depthPixels = new byte[_depthFrameDescription.Width * _depthFrameDescription.Height];

         // create the bitmap to display
         _depthBitmap = new WriteableBitmap(_depthFrameDescription.Width, _depthFrameDescription.Height, 96.0, 96.0, PixelFormats.Gray8, null);

         // set IsAvailableChanged event notifier
         _kinectSensor.IsAvailableChanged += Sensor_IsAvailableChanged;

         // open the sensor
         _kinectSensor.Open();

         // set the status text
         StatusText = _kinectSensor.IsAvailable ? Properties.Resources.RunningStatusText
                                                         : Properties.Resources.NoSensorStatusText;

         // use the window object as the view model in this simple example
         DataContext = this;

         // initialize the components (controls) of the window
         InitializeComponent();
      }

      /// <summary>
      /// INotifyPropertyChangedPropertyChanged event to allow window controls to bind to changeable data
      /// </summary>
      public event PropertyChangedEventHandler PropertyChanged;

      /// <summary>
      /// Gets the bitmap to display
      /// </summary>
      public ImageSource ImageSource => _depthBitmap;

      public int MaxDistance {
         get {
            return _maxDistance;
         }

         set {
            if (_maxDistance != value) {
               _maxDistance = value;
               PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(MaxDistance)));
            }
         }
      }

      public int MinDistance {
         get {
            return _minDistance;
         }

         set {
            if (_minDistance != value) {
               _minDistance = value;
               PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(MinDistance)));
            }
         }
      }

      public double Certainty {
         get {
            return _certainty;
         }

         set {
            if (_certainty != value) {
               _certainty = value;
               PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Certainty)));
            }
         }
      }

      /// <summary>
      /// Add this value to each depth pixel of every even depth image line
      /// </summary>
      public double EvenLineCorrection {
         get {
            return _evenLineCorrection;
         }

         set {
            if (_evenLineCorrection != value) {
               _evenLineCorrection = value;
               PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(EvenLineCorrection)));
            }
         }
      }

      /// <summary>
      /// Gets or sets the current status text to display
      /// </summary>
      public string StatusText {
         get {
            return _statusText;
         }

         set {
            if (_statusText != value) {
               _statusText = value;

               // notify any bound elements that the text has changed
               if (PropertyChanged != null) {
                  PropertyChanged(this, new PropertyChangedEventArgs(nameof(StatusText)));
               }
            }
         }
      }

      /// <summary>
      /// Execute shutdown tasks
      /// </summary>
      /// <param name="sender">object sending the event</param>
      /// <param name="e">event arguments</param>
      private void MainWindow_Closing(object sender, CancelEventArgs e) {
         if (_depthFrameReader != null) {
            // DepthFrameReader is IDisposable
            _depthFrameReader.Dispose();
            _depthFrameReader = null;
         }

         if (_kinectSensor != null) {
            _kinectSensor.Close();
            _kinectSensor = null;
         }
      }

      /// <summary>
      /// Handles the user clicking on the screenshot button
      /// </summary>
      /// <param name="sender">object sending the event</param>
      /// <param name="e">event arguments</param>
      private void ScreenshotButton_Click(object sender, RoutedEventArgs e) {
         if (_depthBitmap != null) {
            // create a png bitmap encoder which knows how to save a .png file
            BitmapEncoder encoder = new PngBitmapEncoder();

            // create frame from the writable bitmap and add to encoder
            encoder.Frames.Add(BitmapFrame.Create(_depthBitmap));

            string time = System.DateTime.Now.ToString("yy'-'MM'-'dd'-'HH'-'mm'-'ss", CultureInfo.CurrentCulture.DateTimeFormat);

            string myPhotos = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);

            string path = Path.Combine(myPhotos, "KinectScreenshot-Depth-" + time + ".png");

            // write the new file to disk
            try {
               // FileStream is IDisposable
               using (FileStream fs = new FileStream(path, FileMode.Create)) {
                  encoder.Save(fs);
               }

               StatusText = string.Format(CultureInfo.CurrentCulture, Properties.Resources.SavedScreenshotStatusTextFormat, path);
            }
            catch (IOException) {
               StatusText = string.Format(CultureInfo.CurrentCulture, Properties.Resources.FailedScreenshotStatusTextFormat, path);
            }
         }
      }

      private void WriteMeshFile() {
         string myDocs = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
         string time = System.DateTime.Now.ToString("yy'-'MM'-'dd'-'HH'-'mm'-'ss", CultureInfo.CurrentCulture.DateTimeFormat);
         string path = Path.Combine(myDocs, "KinectMesh-" + time + ".stl");
         byte[] data = ConvertPointCloudToSTLData(
            _camSpaceAverage, _depthFrameDescription.Width, _depthFrameDescription.Height,
            _minDistance / 1000.0, _maxDistance / 1000.0);
         try {
            File.WriteAllBytes(path, data);
            StatusText = string.Format(CultureInfo.CurrentCulture, Properties.Resources.SavedMeshFileStatusTextFormat, path);
         }
         catch (IOException) {
            StatusText = string.Format(CultureInfo.CurrentCulture, Properties.Resources.FailedToWriteMeshFileStatusTextFormat, path);
         }
      }

      [StructLayout(LayoutKind.Sequential, Pack = 1)]
      struct Vector3DF {

         public Vector3DF(Vector3D vect) {
            X = (float) vect.X; Y = (float) vect.Y; Z = (float) vect.Z;
         }

         public float X;
         public float Y;
         public float Z;
      }

      [StructLayout(LayoutKind.Sequential, Pack = 1)]
      struct STLTriangle {

         public STLTriangle(Vector3D vert1, Vector3D vert2, Vector3D vert3) {
            VectNormal = new Vector3DF();
            Vert1 = new Vector3DF(vert1);
            Vert2 = new Vector3DF(vert2);
            Vert3 = new Vector3DF(vert3);
            AttrByteCount = 0;
         }

         public Vector3DF VectNormal;
         public Vector3DF Vert1;
         public Vector3DF Vert2;
         public Vector3DF Vert3;
         public UInt16 AttrByteCount;

         public static int Size => Marshal.SizeOf(typeof(STLTriangle));
      }

      private static byte[] StructToByteArray<T>(T s) where T: struct {
         int size = Marshal.SizeOf(s);
         byte[] result = new byte[size];

         IntPtr ptr = Marshal.AllocHGlobal(size);
         Marshal.StructureToPtr(s, ptr, true);
         Marshal.Copy(ptr, result, 0, size);
         Marshal.FreeHGlobal(ptr);
         return result;
      }

      private const double MaxPointDistance = 0.1;   // 10cm max distance between vertexes when generating triangles for the STL file
      /// <summary>
      /// ...
      /// </summary>
      /// <param name="pointCloud"></param>
      /// <param name="width"></param>
      /// <param name="height"></param>
      /// <param name="zMin"></param>
      /// <param name="zMax"></param>
      /// <returns></returns>
      private byte[] ConvertPointCloudToSTLData(Vector3D[] pointCloud, int width, int height, double zMin, double zMax) {
         var triangles = new List<STLTriangle>();

         for (int j = 0; j < height - 1; ++j) {
            for (int i = 0; i < width - 1; ++i) {
               Vector3D p00 = _camSpaceAverage[j * width + i];
               Vector3D p10 = _camSpaceAverage[j * width + i + 1];
               Vector3D p01 = _camSpaceAverage[(j + 1) * width + i];
               Vector3D p11 = _camSpaceAverage[(j + 1) * width + i + 1];

               // Top left triangle consisting of p00, p10 and p01:
               if ((p00.DistanceTo(p10) < MaxPointDistance) && (p10.DistanceTo(p01) < MaxPointDistance) && (p01.DistanceTo(p00) < MaxPointDistance)
                  && IsInRange(p00, zMin, zMax) && IsInRange(p10, zMin, zMax) && IsInRange(p01, zMin, zMax)
                  && (p00.Certainty >= Certainty) && (p10.Certainty >= Certainty) && (p01.Certainty >= Certainty)) {
                  triangles.Add(new STLTriangle(p00, p10, p01));
               }

               // Bottom right triangle consisting of p10, p11 and p01:
               if ((p10.DistanceTo(p11) < MaxPointDistance) && (p11.DistanceTo(p01) < MaxPointDistance) && (p01.DistanceTo(p10) < MaxPointDistance)
                  && IsInRange(p10, zMin, zMax) && IsInRange(p11, zMin, zMax) && IsInRange(p01, zMin, zMax)
                  && (p10.Certainty >= Certainty) && (p11.Certainty >= Certainty) && (p01.Certainty >= Certainty)) {
                  triangles.Add(new STLTriangle(p10, p11, p01));
               }
            }
         }

         byte[] result = new byte[84 + triangles.Count * STLTriangle.Size];                                 // 84: 80 bytes (empty) header + number of triangles (UINT32)
         Array.ConstrainedCopy(BitConverter.GetBytes((UInt32) triangles.Count), 0, result, 80, 4);          // Number of triangles
         var concatenatedData = triangles.SelectMany(t => StructToByteArray(t)).ToArray();
         Array.ConstrainedCopy(concatenatedData, 0, result, 84, concatenatedData.Length);
         return result;
      }

      /// <summary>
      /// Handles the depth frame data arriving from the sensor
      /// </summary>
      /// <param name="sender">object sending the event</param>
      /// <param name="e">event arguments</param>
      private void DepthFrameArrived(object sender, DepthFrameArrivedEventArgs e) {
         bool depthDataProcessed = false;

         using (DepthFrame depthFrame = e.FrameReference.AcquireFrame()) {
            if (depthFrame != null) {
               // the fastest way to process the body index data is to directly access 
               // the underlying buffer
               using (KinectBuffer depthBuffer = depthFrame.LockImageBuffer()) {
                  // verify data and write the color data to the display bitmap
                  if (((_depthFrameDescription.Width * _depthFrameDescription.Height) == (depthBuffer.Size / _depthFrameDescription.BytesPerPixel)) &&
                      (_depthFrameDescription.Width == _depthBitmap.PixelWidth) && (_depthFrameDescription.Height == _depthBitmap.PixelHeight)) {

                     // Convert depth image to xyz coordinates
                     _kinectSensor.CoordinateMapper.MapDepthFrameToCameraSpaceUsingIntPtr(depthBuffer.UnderlyingBuffer, depthBuffer.Size, _camSpacePoints);
                     ProcessCameraSpaceData();
                     GenerateDepthImageFromCamSpaceData();
                     depthDataProcessed = true;
                  }
               }
            }
         }

         if (depthDataProcessed) {
            RenderDepthPixels();
         }
      }

      long _frameNumber = 1;
      /// <summary>
      /// ...
      /// </summary>
      private void ProcessCameraSpaceData() {
         double zMin = _minDistance / 1000.0;
         double zMax = _maxDistance / 1000.0;
         long largeChanges = 0;

         Parallel.For(0, _camSpacePoints.Length, (i) => {
            CameraSpacePoint p = _camSpacePoints[i];
            Vector3D valNew = new Vector3D(p.X, p.Y, p.Z);

            // Even line depth correction
            if ((valNew.IsValid) && ((i / _depthFrameDescription.Width) & 1) == 0) {
               valNew.Z += EvenLineCorrection / 1000.0;
            }

            if (_frameNumber == 1) {
               if (IsInRange(valNew, zMin, zMax)) _camSpaceAverage[i] = valNew;
               else _camSpaceAverage[i] = new Vector3D(double.NegativeInfinity, double.NegativeInfinity, double.NegativeInfinity);
            }
            else {
               Vector3D valOld = _camSpaceAverage[i];
               Vector3D valAvg;
               if (valNew.IsValid) {   // Valid new value is present
                  if (!valOld.IsValid) {
                     if (IsInRange(valNew, zMin, zMax)) valAvg = valNew;
                     else valAvg = valOld;
                  }
                  else {
                     double f = (double) (_frameNumber - 1) / _frameNumber;
                     double x = f * valOld.X + (valNew.X / _frameNumber);
                     double y = f * valOld.Y + (valNew.Y / _frameNumber);
                     double z = f * valOld.Z + (valNew.Z / _frameNumber);
                     valAvg = new Vector3D(x, y, z, valOld.Certainty);

                     // Decrement certainty if new value is outside the depth range
                     if (!IsInRange(valNew, zMin, zMax)) {
                        valAvg.Certainty *= ((double) (_frameNumber - 1) / _frameNumber);
                     }
                  }

                  // Count depth changes larger than 2 cm inside the depth range
                  if ((valOld.Certainty > Certainty) && IsInRange(valOld, zMin, zMax)
                     && (Math.Abs(valOld.Z - valNew.Z) > 0.02)) {
                     Interlocked.Increment(ref largeChanges); 
                  }
               }
               else {
                  // Just decrease the certainty of the current value if no new value is present
                  valAvg = valOld;
                  if (valAvg.IsValid) valAvg.Certainty = valOld.Certainty * ((double) (_frameNumber - 1) / _frameNumber);
               }
               _camSpaceAverage[i] = valAvg;
            }
         });

         ++_frameNumber;
         if (largeChanges > 15000) _frameNumber = 1;  // Reset everything if the scene has changed
      }

      /// <summary>
      /// ...
      /// </summary>
      /// <param name="depthFrameData">Pointer to the DepthFrame image data</param>
      /// <param name="depthFrameDataSize">Size of the DepthFrame image data</param>
      /// <param name="minDepth">The minimum reliable depth value for the frame</param>
      /// <param name="maxDepth">The maximum reliable depth value for the frame</param>
      private void GenerateDepthImageFromCamSpaceData() {
         double zMin = _minDistance / 1000.0;
         double zMax = _maxDistance / 1000.0;
         double mapDepthToByte = 255.0 / (zMax - zMin);

         // convert depth to a visual representation
         Parallel.For(0, _camSpaceAverage.Length, (i) => {
            Vector3D p = _camSpaceAverage[i];
            if ((p.Certainty >= Certainty) && (p.Z >= zMin) && (p.Z <= zMax)) {
               _depthPixels[i] = (byte) ((p.Z - zMin) * mapDepthToByte);
            }
            else _depthPixels[i] = 0;
         });
      }

      /// <summary>
      /// Renders color pixels into the writeableBitmap.
      /// </summary>
      private void RenderDepthPixels() {
         _depthBitmap.WritePixels(
             new Int32Rect(0, 0, _depthBitmap.PixelWidth, _depthBitmap.PixelHeight),
             _depthPixels,
             _depthBitmap.PixelWidth,
             0);
      }

      /// <summary>
      /// Handles the event which the sensor becomes unavailable (E.g. paused, closed, unplugged).
      /// </summary>
      /// <param name="sender">object sending the event</param>
      /// <param name="e">event arguments</param>
      private void Sensor_IsAvailableChanged(object sender, IsAvailableChangedEventArgs e) {
         // on failure, set the status text
         StatusText = _kinectSensor.IsAvailable ? Properties.Resources.RunningStatusText
                                                         : Properties.Resources.SensorNotAvailableStatusText;
      }

      private void ButtonReset_Click(object sender, RoutedEventArgs e) {
         _frameNumber = 1;
      }

      private void SldMaxDist_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e) {
         _frameNumber = 1;
      }

      private void SldMinDist_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e) {
         _frameNumber = 1;
      }

      private void SldCertainty_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e) {
         _frameNumber = 1;
      }

      private void SldLineCorr_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e) {
         _frameNumber = 1;
      }

      private void STLScrnShotButton_Click(object sender, RoutedEventArgs e) {
         WriteMeshFile();
      }

      private bool IsInRange(Vector3D v, double zMin, double zMax) => v.IsValid && (v.Z >= zMin) && (v.Z <= zMax);

      private void Image_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e) {
         Point p = e.GetPosition(_image);
         int i = (int) _depthBitmap.Width * (int) p.Y + (int) p.X;
         StatusText = $"Point ({(int) p.X}, {(int) p.Y}): Certainty = {_camSpaceAverage[i].Certainty:F2}, Z = {_camSpaceAverage[i].Z:F3}";
      }
   }
}
