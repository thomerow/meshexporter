﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeshExporter {

   struct Vector3D {

      public Vector3D(double x, double y, double z) {
         X = x;
         Y = y;
         Z = z;
         Certainty = double.IsNegativeInfinity(x) ? 0.0 : 1.0;
      }

      public Vector3D(double x, double y, double z, double certainty) {
         X = x;
         Y = y;
         Z = z;
         Certainty = certainty;
      }

      public Vector3D(float x, float y, float z) {
         X = x;
         Y = y;
         Z = z;
         Certainty = float.IsNegativeInfinity(x) ? 0.0 : 1.0;
      }

      //
      // Summary:
      //     The X coordinate of the vector.
      public double X;
      //
      // Summary:
      //     The Y coordinate of the vector.
      public double Y;
      //
      // Summary:
      //     The Z coordinate of the vector.
      public double Z;

      /// <summary>
      /// ...
      /// </summary>
      public double Certainty;

      /// <summary>
      /// ...
      /// </summary>
      public bool IsValid => X > double.NegativeInfinity;

      /// <summary>
      /// ...
      /// </summary>
      /// <param name="other"></param>
      /// <returns></returns>
      public double DistanceTo(Vector3D other) {
         if (!IsValid || !other.IsValid) return double.PositiveInfinity;
         double dX = other.X - X;
         double dY = other.Y - Y;
         double dZ = other.Z - Z;
         return Math.Sqrt(dX * dX + dY * dY + dZ * dZ);
      }
   }
}
